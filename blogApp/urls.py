from django.urls import path
from . import views

# view loaded for specific path and name of url defined
#include url path for post detail with primary key as unique identifier (pk)

urlpatterns = [
    path('',views.post_list,name='post_list'),
    path('post/<int:pk>/',views.post_detail,name='post_detail'),
    path('post/new/',views.post_new,name='post_new'),
    path('post/<int:pk>/edit/',views.post_edit,name='post_edit'),
    path('posts/<int:page>/',views.post_list,name='post_list'),
    path('post.json',views.listing_api,name='post-api'),
]