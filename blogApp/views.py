from django.core.paginator import Paginator
from django.http import JsonResponse
from django.shortcuts import redirect, render
from .models import Post
from django.utils import timezone
from django.shortcuts import get_object_or_404
from .forms import PostForm

# Create your views here.

def listing_api(request):
    page_number = request.GET.get('page', 1)
    per_page = request.GET.get('per_page', 2)
    startswith = request.GET.get('startswith', '')
    posts = Post.objects.filter(
        title__startswith=startswith
    )
    paginator = Paginator(posts, per_page)
    page_obj = paginator.get_page(page_number)
    data = [{'title': post.title, 'description': post.description, 'published_date':post.published_date} for post in page_obj.object_list]

    payload ={
        "page": {
            "current": page_obj.number,
            "has_next": page_obj.has_next(),
            "has_previous": page_obj.has_previous(),
        },
        "data": data
    }

    return JsonResponse(payload)


def post_list(request,page=1):
    posts=Post.objects.filter(published_date__lte=timezone.now()).order_by('-published_date')
    paginator = Paginator(posts, per_page=3)
    page_object = paginator.get_page(page)
    context={"page_obj": page_object}

    return render(request, 'blog/post_list.html',context)
    
def post_detail(request,pk):
    post=get_object_or_404(Post,pk=pk)
    return render(request, 'blog/post_detail.html',{'post':post})

def post_new(request):
    # create a form instance with data from the POST request
    if(request.method=='POST'):
        form=PostForm(request.POST)  
    
        if form.is_valid():
            post=form.save(commit=False)
            post.author=request.user
            post.published_date=timezone.now()
            post.save()
    
        return redirect('post_detail',pk=post.pk)

    else:
        form=PostForm()           
        return render(request, 'blog/post_new.html',{'form':form})

def post_edit(request,pk):
    # post_new template used for editing a post
    post=get_object_or_404(Post,pk=pk)
    if request.method=='POST':
        form=PostForm(request.POST,instance=post)

        if form.is_valid():
            form.save(commit=False)
            form.author=request.user
            form.published_date=timezone.now()
            form.save()
    else:
        form=PostForm(instance=post)
    return render(request, 'blog/post_new.html',{'form':form})
        

        
